import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class GUI extends JFrame implements ActionListener{
	
	private Day day1 = new Day();
	private JTextField day = new JTextField("enter the current day in lowercase");
	private JTextField adddays = new JTextField("enter the number of days you would like to add");
	JButton setdayButton = new JButton( new AbstractAction("Set Day") {
		@Override
		public void actionPerformed( ActionEvent e ) {
			day1.setday(day.getText());
		}
	});
	
	
	JButton nextdayButton = new JButton( new AbstractAction("next day") {
		@Override
		public void actionPerformed( ActionEvent e ) {
			JOptionPane.showMessageDialog(null,"The next day is " + day1.getnextday());
		}
	});
	
	
	JButton prevdayButton = new JButton( new AbstractAction("previous day") {
		@Override
		public void actionPerformed( ActionEvent e ) {
			JOptionPane.showMessageDialog(null,"The previous day is " + day1.getprevday());
		}
	});
	
	
	JButton currentdayButton = new JButton( new AbstractAction("current day") {
		@Override
		public void actionPerformed( ActionEvent e ) {
			JOptionPane.showMessageDialog(null,"The current day is " + day1.getday());  
		}
	});
	
	
	JButton adddaysButton = new JButton( new AbstractAction("add days") {
		@Override
		public void actionPerformed( ActionEvent e ) {
			day1.adddays(Integer.parseInt(adddays.getText()));
		}
	});

	
	private JPanel panel1;
	

	public GUI(){
		panel1 = new JPanel();
		panel1.setBackground(Color.DARK_GRAY);

		panel1.add(day);
		panel1.add(setdayButton);
		panel1.add(prevdayButton);
		panel1.add(currentdayButton);
		panel1.add(nextdayButton);
		panel1.add(adddays);
		panel1.add(adddaysButton);


		add(panel1, BorderLayout.CENTER);
		
		
		setSize(320,180);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setResizable(true);
	}

	public static void main(String[] args) {
		GUI x = new GUI();
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}





}



