
public class Day {
	private String days[] = {"monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"};
	private int daynum = 0;
	private int totaldays = 0;


	public Day() {
		this.daynum = 0;

	}
	public Day(String days){
		if (days.equals(this.days[0])){
			this.daynum = 0;
		}
		if (days.equals(this.days[1])){
			this.daynum = 1;
		}
		if (days.equals(this.days[2])){
			this.daynum = 2;
		}
		if (days.equals(this.days[3])){
			this.daynum = 3;
		}
		if (days.equals(this.days[4])){
			this.daynum = 4;
		}
		if (days.equals(this.days[5])){
			this.daynum = 5;
		}
		if (days.equals(this.days[6])){
			this.daynum = 6;
		}

	}
	public String getday(){
		return days[daynum];
	}
	public String getnextday(){
		if (daynum > 5){return days[0];}
		else{ return days[daynum + 1];}
	}
	public String getprevday(){
		if (daynum < 1){return days[6];}
		else{ return days[daynum - 1];}
	}
	public void setday(String days ){
		if (days.equals(this.days[0])){
			this.daynum = 0;
		}
		if (days.equals(this.days[1])){
			this.daynum = 1;
		}
		if (days.equals(this.days[2])){
			this.daynum = 2;
		}
		if (days.equals(this.days[3])){
			this.daynum = 3;
		}
		if (days.equals(this.days[4])){
			this.daynum = 4;
		}
		if (days.equals(this.days[5])){
			this.daynum = 5;
		}
		if (days.equals(this.days[6])){
			this.daynum = 6;
		}
	}
	public void adddays(int plusdays){
		
		totaldays = daynum + plusdays;
		this.daynum = totaldays % 7;
	}	
	public void printday(){
		System.out.println("the day is " + days[daynum]);
	}
	
}
