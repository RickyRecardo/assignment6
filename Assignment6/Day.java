
public class Day {
	private String days[] = {"monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"};
	private int daynum = 0;
	private int totaldays = 0;

/**
 * this us the no input arguments constructor
 */
	public Day() {
		this.daynum = 0;

	}
	/**
	 * this is the overloaded constructor, for the input you enter the day of the week as a string
	 * @param days
	 */
	public Day(String days){
		if (days.equals(this.days[0])){
			this.daynum = 0;
		}
		if (days.equals(this.days[1])){
			this.daynum = 1;
		}
		if (days.equals(this.days[2])){
			this.daynum = 2;
		}
		if (days.equals(this.days[3])){
			this.daynum = 3;
		}
		if (days.equals(this.days[4])){
			this.daynum = 4;
		}
		if (days.equals(this.days[5])){
			this.daynum = 5;
		}
		if (days.equals(this.days[6])){
			this.daynum = 6;
		}

	}
	/**
	 * will return the day of the week
	 * @return
	 */
	public String getday(){
		return days[daynum];
	}
	/**
	 * will return the next day of the week
	 * @return
	 */
	public String getnextday(){
		if (daynum > 5){return days[0];}
		else{ return days[daynum + 1];}
	}
	/**
	 * will return the previous day of the week
	 * @return
	 */
	public String getprevday(){
		if (daynum < 1){return days[6];}
		else{ return days[daynum - 1];}
	}
	/**
	 * allows the user to set the day on an already made object
	 * @param days
	 */
	public void setday(String days ){
		if (days.equals(this.days[0])){
			this.daynum = 0;
		}
		if (days.equals(this.days[1])){
			this.daynum = 1;
		}
		if (days.equals(this.days[2])){
			this.daynum = 2;
		}
		if (days.equals(this.days[3])){
			this.daynum = 3;
		}
		if (days.equals(this.days[4])){
			this.daynum = 4;
		}
		if (days.equals(this.days[5])){
			this.daynum = 5;
		}
		if (days.equals(this.days[6])){
			this.daynum = 6;
		}
	}
	/**
	 * allows the user to add days to the current day
	 * @param plusdays
	 */
	public void adddays(int plusdays){
		
		totaldays = daynum + plusdays;
		this.daynum = totaldays % 7;
	}	
	/**
	 * will print the day to the console
	 */
	public void printday(){
		System.out.println("the day is " + days[daynum]);
	}
	
}
